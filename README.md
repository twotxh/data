# Northern SAC Data

**This data maybe incorrect**

Public data related to the Northern SAC.

## File Status

*Example (ok as of Dec 12, 1234)*: ![`filename`](http://shields.io/static/v1?label=filename&message=1234-12-12&color=success)

*Example (incomplete/incorrect as of Dec 12, 1234)*: ![`filename`](http://shields.io/static/v1?label=filename&message=1234-12-12&color=critical)

*Example (outdated as of Dec 12, 1234)*: ![`filename`](http://shields.io/static/v1?label=filename&message=1234-12-12&color=inactive)

If not listed below, the file is *not to be used as a source of data* or is incomplete/incorrect or is outdated.

___

![`/clubs.json`](http://shields.io/static/v1?label=%2Fclubs.json&message=2020-12-23&color=critical)

![`/budget-act.pdf`](http://shields.io/static/v1?label=%2Fbudget-act.pdf&message=2020-12-23&color=success)

![`/elections-act.pdf`](http://shields.io/static/v1?label=%2Felections-act.pdf&message=2020-12-23&color=success)

![`/constitution.pdf`](http://shields.io/static/v1?label=%2Fconstitution.pdf&message=2020-12-23&color=success)

![`/budgets/budgets.json`](http://shields.io/static/v1?label=%2Fbudgets%2Fbudgets.json&message=2020-12-23&color=success)

![`/budgets/feb-2019.pdf`](http://shields.io/static/v1?label=%2Fbudgets%2Ffeb-2019.pdf&message=2020-12-23&color=success)

![`/budgets/oct-2019.pdf`](http://shields.io/static/v1?label=%2Fbudgets%2Foct-2019.pdf&message=2020-12-23&color=success)

![`/budgets/oct-2018.pdf`](http://shields.io/static/v1?label=%2Fbudgets%2Foct-2018.pdf&message=2020-12-23&color=success)

![`/minutes/minutes.json`](http://shields.io/static/v1?label=%2Fminutes%2Fminutes.json&message=2020-12-23&color=critical)
